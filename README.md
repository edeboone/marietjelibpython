# pyMarietjeLib

This is supposed to become a python library for interfacing with the Marietje Soundsystem in de Zuidkantine.
It should handle the credentials needed to access the api at [https://marietje.marie-curie.nl](https://marietje.marie-curie.nl).
Furthermore, to decrease load on the api, a datacache might be implemented.

Note that this library is planned to be co-developed with the [Marietje MPRIS server](https://gitlab.science.ru.nl/edeboone/marietjeprispy).
We'll see how it works out.
