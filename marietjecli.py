#!/usr/bin/env python3
from marietjelibpy import Marietje


def main(marietje, config):

    print("Logging in...")
    if ( m.login(user_config.username, user_config.password) ):
        print("Logged in as: " + m.whoami())
    else:
        print("Logging in failed!")
        print("We should be expecting a LoginException.")

    print("Fetching the current track")
    print(m.get_current_song())



if __name__ == "__main__":
    # This is only a small test for the lib
    # It might be seen as a client (and its responsibilities)
    import user_config

    m = Marietje()

    print("Using {name} {version}.".format(name=m.NAME, version=m.VERSION))

    main(m, user_config)


