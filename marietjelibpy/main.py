from functools import wraps

import datetime
import requests

# Package Settings
from . import settings

def with_session(func):
    @wraps(func)
    def _wrapper(self, *args, **kwargs):
        if not self.session:
            self.session = requests.Session()

        return func(self, *args, **kwargs)

    return _wrapper

def login_required(func):
    @wraps(func)
    def _wrapper(self, *args, **kwargs):
        now = datetime.datetime.now()
        if not self.loggedin or now > self.loggedin:
            self.session = None
            self.login()

        return func(self, *args, **kwargs)
    return _wrapper

def cached(func):
    @wraps(func)
    def _wrapper(self, *args, **kwargs):
        now = datetime.datetime.now()

        name = func.__name__

        if name in self.cache:
            if self.cache[name]['time'] > now:
                return self.cache[name]['value']

        result = func(self, *args, **kwargs)

        self.cache[name] = {
                'time': now + datetime.timedelta(seconds=int(settings.DATA_CACHE)),
                'value': result
        }

        return result
    return _wrapper


class LoginException(Exception):
    pass

class Main:
    NAME = "MarietjeLibPython"
    VERSION = '0.1'

    def __init__(self, config=None):
        self.session = None
        self.loggedin = False
        self.cookies = None
        self.cache = {}
        self.credentials = {
                'username': config['username'],
                'password': config['password']
                }

    def _update_credential_cache(self, cookies):
        self.loggedin = datetime.datetime.now() + datetime.timedelta(seconds=int(settings.CREDENTIAL_CACHE))
        self.cookies = cookies

    @with_session
    def login(self, username=None, password=None):
        # Get an initial CSRF token
        init_login_response = self.session.get(settings.INIT_LOGIN_URL)

        cookies = dict( csrftoken = init_login_response.cookies['csrftoken'] )
        headers = {
                'referer': settings.INIT_LOGIN_URL,
                'X-CSRFToken': cookies['csrftoken']
            }
        payload = {
                'username': username if username is not None else self.credentials['username'],
                'password': password if password is not None else self.credentials['password']
            }

        # Try to login
        login_response = self.session.post(settings.LOGIN_URL, data=payload, headers=headers, cookies=cookies)

        if not login_response:
            raise LoginException(login_response)
            return False

        self._update_credential_cache(cookies)
        return True

    def logout(self):
        if not self.session:
            return

        self.session.post(settings.LOGOUT_URL)
        self.session = None

    @login_required
    @cached
    def get_general_info(self):
        payload = {
                "all": True,
                "page": 0,
                "pagesize": 10,
                "X-CSRFToken": self.cookies['csrftoken']
            }

        header = {
                'X-CSRFToken': self.cookies['csrftoken']
            }

        response = self.session.get(settings.QUEUE_URL, data=payload, headers=header, cookies=self.cookies)

        return response.json()

    def whoami(self):
        data = self.get_general_info()

        return data['user_name']

    def get_current_song(self):
        data = self.get_general_info()

        return data['current_song']
