BASE_URL = 'https://marietje.marie-curie.nl'
API_URL =  BASE_URL + '/api'
INIT_LOGIN_URL = BASE_URL + '/login'
LOGIN_URL = API_URL + '/login'
LOGOUT_URL = API_URL + '/logout'
QUEUE_URL = API_URL + '/queue'
NEXT_URL = API_URL + '/next'

CREDENTIAL_CACHE = "300"
CREDENTIAL_FILE = "credentials"
DATA_CACHE = "5"
